source config.bash

#libfreenect
echo "Downloading and compiling libfreenect 0.4.3"
cd ${SOURCE_LIBRARIES_DIR}
git clone https://github.com/OpenKinect/libfreenect.git
cd libfreenect
#Building
echo "Building libfreenect"
mkdir build
cd build
cmake .. -DCMAKE_BUILD_TYPE=Release
make
echo "Done"
