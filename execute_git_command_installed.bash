source config.bash
CURRENT_DIR=`pwd`

## srrg_packages
echo "Executing $* on installed SRRG packages"

for package in ${SOURCE_SRRG_DIR}/srrg*
do
  echo "Executing git command $* on  ${package}"
  cd ${package}
  git $*
done

echo " Done"
cd $CURRENT_DIR
